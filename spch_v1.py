import speech_recognition as sr 
import pyaudio
import wave
import pyttsx3
#speech to text
import sounddevice as sd
from scipy.io.wavfile import write
import os
eng = pyttsx3.init()

eng.say("System turned ON")
eng.say("Welcome to Blinds Vision Fist")
eng.runAndWait()

def my_specch_rec() :
#speech
 chunk = 1024  # Record in chunks of 1024 samples
 sample_format = pyaudio.paInt16  # 16 bits per sample
 channels = 2
 fs = 44100  # Record at 44100 samples per second
 seconds = 5
 filename = "output.wav"
 p = pyaudio.PyAudio()  # Create an interface to PortAudio

 eng.say("what can i search for you?")
 eng.runAndWait()
 
 print("Starting: Speak now!")
 stream = p.open(format=sample_format,
                channels=channels,
                rate=fs,
                frames_per_buffer=chunk,
                input=True)
 frames = []  # Initialize array to store frames

 # Store data in chunks for 5 seconds
 for i in range(0, int(fs / chunk * seconds)):
       data = stream.read(chunk)
       frames.append(data)

 stream.stop_stream()
 stream.close()
 p.terminate()
 print('Finished recording')
 eng.say("Gathering Information...")
 eng.runAndWait()
 
# Save the recorded data as a WAV file
 wf = wave.open(filename, 'wb')
 wf.setnchannels(channels)
 wf.setsampwidth(p.get_sample_size(sample_format))
 wf.setframerate(fs)
 wf.writeframes(b''.join(frames))
 wf.close()

 AUDIO_FILE = ("output.wav")
 r = sr.Recognizer()
 with sr.AudioFile(AUDIO_FILE) as source: 
    #reads the audio file. Here we use record instead of 
    #listen 
      audio = r.record(source)
      str = "" 	 
      str= r.recognize_google(audio,language = 'en-IN', show_all=False)
 print(str)
 str=str.strip()
 print("The audio file contains: " +str )
 return (str)
	
	
def check_if_string_in_file(file_name, string_to_search):
    """ Check if model contains label"""
    # Open the file in read only mode
    with open(file_name, 'r') as read_obj:
        # Read all lines in the file one by one
        for line in read_obj:
            # For each line, check if line contains the string
            if string_to_search == line.rstrip():
                return True
    return False	
flg = True
while flg:
 str=my_specch_rec()
 if check_if_string_in_file('labelmap.txt', str):
     eng.say("You searched for : ")
     eng.say(str)
     eng.runAndWait()
     
     print('Yes, string found in file',str)
     flg=False
     strng= str
 else:
     eng.say("You searched for : ")
     eng.say(str)
     eng.say("But unfortunately i can't find it. Search for different object")  
     eng.runAndWait()        
     print('String not found in file')
