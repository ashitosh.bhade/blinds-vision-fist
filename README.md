# Introduction

``TensorFlow Lite is an optimized framework for deploying lightweight deep learning models on resource-constrained edge devices. 
TensorFlow Lite models have faster inference time and require less processing power, so they can be used to obtain faster performance in realtime applications.``

# Setup 
Setting up TensorFlow Lite on the Raspberry Pi

1. Update the Raspberry Pi

  * `sudo apt-get update`
  * `sudo apt-get dist-upgrade`
   
2. Install TensorFlow Lite dependencies and OpenCV and speech recognition and text to speech.
  
3. run shell script that will automatically download and install all the packages and dependencies
   
  * `bash get_requirements.sh`

4. Set up TensorFlow Lite detection model
  
  Google's sample TFLite model
   
  * `wget https://storage.googleapis.com/download.tensorflow.org/models/tflite/coco_ssd_mobilenet_v1_1.0_quant_2018_06_29.zip` 
    
  * `unzip coco_ssd_mobilenet_v1_1 0_quant_2018_06_29.zip -d Sample_TFLite_model`

5. create virtual environment

  * `sudo pip3 install virtualenv`
  * `python3 -m venv bvf-env`
  
# Run The Project
  
* Activate the virtual environment (optional)
 
  `source bvf-env/bin/activate`
  
* Run the BVF TensorFlow Lite model!
  
  `python3 blinds_vision_fist.py --modeldir=Sample_TFLite_model`